$(document).ready(function() {
    var tipoEvento = ((document.ontouchstart !== null) ? 'click' : 'touchstart');
    /*-------------------*/
    var limite = 0;
    /*----------------------*/
    $(".enviar_apartar").on(tipoEvento, function(e) {
        e.preventDefault();
        $(".enviar_apartar").attr('disabled', true);
        $("#form_apartar").submit();
    });
    /*------------------*/
    $(".apartamento").change(function() {
        $(".hora").html('');
        var apartamento = $(this).val();
        var salon = $(".salon").val();
        var fecha = $(".fecha").val();
        validarDiasSemana(apartamento, fecha, salon);
    });
    /*-------------------*/
    $(".salon").change(function() {
        $(".hora").html('');
        var salon = $(this).val();
        var apartamento = $(".apartamento").val();
        var fecha = $(".fecha").val();
        validarDiasSemana(apartamento, fecha, salon);
    });
    /*-------------------*/
    $(".fecha").change(function() {
        $(".hora").html('');
        var fecha = $(this).val();
        var apartamento = $(".apartamento").val();
        var salon = $(".salon").val();
        /*-----------------*/
        var today = new Date();
        var dd = (today.getDate() < 10) ? '0' + today.getDate() : today.getDate();
        var mm = (today.getMonth() + 1 < 10) ? '0' + today.getMonth() + 1 : today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        today = yyyy + '-' + mm + '-' + dd;
        /*----------------------------*/
        if (fecha < today) {
            ohSnap("No puedes reservar para fechas anteriores", {
                color: "red",
                "duration": "1000"
            });
            $(this).val('');
        } else {
            validarDiasSemana(apartamento, fecha, salon);
        }
        /*-----------------*/
    });
    /*-------------------*/
    function validarDiasSemana(apartamento, fecha, salon) {
        try {
            $.ajax({
                url: '../vistas/ajax/salon/validarSemana.php',
                method: 'POST',
                data: {
                    'apartamento': apartamento,
                    'salon': salon,
                    'fecha': fecha,
                },
                dataType: 'json',
                cache: false,
                success: function(resultado) {
                    var horas_restantes = resultado.horas_restantes;
                    if (resultado.mensaje == 'semana') {
                        ohSnap("Limite de reservas semanal cumplido para este salon", {
                            color: "red",
                            "duration": "1000"
                        });
                        $(".salon").val('');
                        $(".salon").focus();
                        $(".fecha").val('');
                        $(".hora").hide();
                        horas_restantes = 0;
                    } else
                    /*---------------------------*/
                    if (resultado.mensaje == 'horas' || horas_restantes == 0) {
                        ohSnap("Limite de reservas diario cumplido para este salon", {
                            color: "red",
                            "duration": "1000"
                        });
                        $(".salon").val('');
                        $(".fecha").val('');
                        $(".hora").hide();
                    } else
                    /*---------------------------*/
                    if (resultado.mensaje == 'fecha') {
                        ohSnap("No puedes realizar la reserva para este dia", {
                            color: "red",
                            "duration": "1000"
                        });
                        $(".fecha").val('');
                        $(".hora").hide();
                        horas_restantes = 0;
                    } else
                    /*---------------------------*/
                    if (fecha != '' && salon != "") {
                        if (horas_restantes != 0) {
                            $(".hora").show()
                        } else {
                            $(".hora").hide()
                        }
                        mostrarHoras(apartamento, salon, fecha, horas_restantes)
                    }
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    /*-----------------------------------*/
    function mostrarHoras(apartamento, salon, fecha, horas) {
        try {
            $.ajax({
                url: '../vistas/ajax/salon/mostrarHoras.php',
                method: 'POST',
                data: {
                    'apartamento': apartamento,
                    'salon': salon,
                    'fecha': fecha,
                },
                cache: false,
                dataType: 'json',
                success: function(r) {
                    var array = r.horas_activas;
                    array.forEach(function(datos) {
                        $(".hora").append('<div class="custom-control custom-switch ml-4">' + '<input type="checkbox" class="custom-control-input hora_check" id="' + datos.id_hora + '" value="' + datos.id_hora + '" name="horas[]">' + '<label class="custom-control-label" for="' + datos.id_hora + '">' + datos.horas + '</label>' + '</div>');
                    });
                    limite = horas;
                    /*-----------------------------------*/
                }
            });
        } catch (evt) {
            alert(evt.message);
        }
    }
    // Evento que se ejecuta al soltar una tecla en el input
    /*-----------------------------------*/
    $("#cantidad").keydown(function() {
        $("input[type=checkbox]").prop('checked', false);
        $("#seleccionados").html("0");
    });
    $('.hora').on(tipoEvento, 'input[type=checkbox]', function() {
        /*        if (limite == 0) {
                    $(".salon").val('');
                    $(".fecha").val('');
                }*/
        // Cogemos el elemento actual
        var elemento = this;
        var contador = 0;
        // Recorremos todos los checkbox para contar los que estan seleccionados
        $("input[type=checkbox]").each(function() {
            if ($(this).is(":checked")) contador++;
        });
        // Comprovamos si supera la cantidad máxima indicada
        if (contador > limite) {
            ohSnap("Limite de horas a reservar " + limite, {
                color: "red",
                "duration": "1000"
            });
            // Desmarcamos el ultimo elemento
            $(elemento).prop('checked', false);
            //contador--;
        }
        $("#seleccionados").html(contador);
    });
});