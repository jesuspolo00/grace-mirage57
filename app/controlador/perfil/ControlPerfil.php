<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'perfil' . DS . 'ModeloPerfil.php';
require_once CONTROL_PATH . 'hash.php';

class ControlPerfil
{

    private static $instancia;

    public static function singleton_perfil()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarDatosPerfilControl($id)
    {
        $datos = ModeloPerfil::mostrarDatosPerfilModel($id);
        return $datos;
    }

    public function mostrarPerfilesControl()
    {
        $datos = ModeloPerfil::mostrarPerfilesModel();
        return $datos;
    }

    public function editarPerfilControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_user']) &&
            !empty($_POST['id_user'])
        ) {

            $pass      = $_POST['password'];
            $conf_pass = $_POST['conf_password'];

            $clavecifrada = ($pass == $conf_pass && $pass != "" && $conf_pass != "") ? Hash::hashpass($pass) : $_POST['pass_old'];

            $datos = array(
                'id_user'   => $_POST['id_user'],
                'documento' => $_POST['documento'],
                'nombre'    => $_POST['nombre'],
                'apellido'  => $_POST['apellido'],
                'correo'    => $_POST['correo'],
                'telefono'  => $_POST['telefono'],
                'pass'      => $clavecifrada,
                'perfil'    => $_POST['perfil'],
            );

            $guardar = ModeloPerfil::editarPerfilModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Modificado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        } else {
            echo '
            <script>
            ohSnap("Ha ocurrido un error!", {color: "red"});
            </script>
            ';
        }
    }

}
