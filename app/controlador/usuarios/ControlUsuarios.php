<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'usuarios' . DS . 'ModeloUsuarios.php';
require_once CONTROL_PATH . 'hash.php';

class ControlUsuarios
{

    private static $instancia;

    public static function singleton_usuario()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function mostrarTodosUsuariosControl()
    {
        $comando = ModeloUsuarios::comandoSQL();
        $mostrar = ModeloUsuarios::mostrarTodosUsuariosModel();
        return $mostrar;
    }

    public function mostrarUsuariosControl()
    {
        $comando = ModeloUsuarios::comandoSQL();
        $mostrar = ModeloUsuarios::mostrarUsuariosModel();
        return $mostrar;
    }

    public function mostrarDatosUsuariosControl($id)
    {
        $comando = ModeloUsuarios::comandoSQL();
        $mostrar = ModeloUsuarios::mostrarDatosUsuariosModel($id);
        return $mostrar;
    }

    public function mostrarUsuariosBuscarControl($buscar)
    {
        $comando = ModeloUsuarios::comandoSQL();
        $mostrar = ModeloUsuarios::mostrarUsuariosBuscarControl($buscar);
        return $mostrar;
    }

    public function agregarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $pass = Hash::hashpass($_POST['documento']);

            $datos = array(
                'id_log'    => $_POST['id_log'],
                'documento' => $_POST['documento'],
                'nombre'    => $_POST['nombre'],
                'apellido'  => $_POST['apellido'],
                'correo'    => $_POST['correo'],
                'telefono'  => $_POST['telefono'],
                'perfil'    => $_POST['perfil'],
                'user'      => $_POST['documento'],
                'pass'      => $pass,
            );

            $guardar = ModeloUsuarios::agregarUsuarioModel($datos);

            if ($guardar['guardar'] == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function editarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_user'   => $_POST['id_user'],
                'id_log'    => $_POST['id_log'],
                'documento' => $_POST['documento_edit'],
                'nombre'    => $_POST['nombre_edit'],
                'apellido'  => $_POST['apellido_edit'],
                'correo'    => $_POST['correo_edit'],
                'telefono'  => $_POST['telefono_edit'],
                'perfil'    => $_POST['perfil_edit'],
            );

            $guardar = ModeloUsuarios::editarUsuarioModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Modificado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function inactivarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $inactivar = ModeloUsuarios::inactivarUsuarioModel($_POST['id']);
            $mensaje   = ($inactivar == true) ? 'ok' : 'no';
            return $mensaje;
        }
    }

    public function activarUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $activar = ModeloUsuarios::activarUsuarioModel($_POST['id']);
            $mensaje = ($activar == true) ? 'ok' : 'no';
            return $mensaje;
        }
    }

    public function restablecerUsuarioControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {

            $pass_new = Hash::hashpass($_POST['documento']);

            $datos = array(
                'id_user' => $_POST['id'],
                'pass'    => $pass_new,
            );

            $restablecer = ModeloUsuarios::restablecerUsuarioModel($datos);
            $mensaje     = ($restablecer == true) ? 'ok' : 'no';
            return $mensaje;
        }
    }

    public function consultarDocumentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id']) &&
            !empty($_POST['id'])
        ) {
            $datos   = ModeloUsuarios::consultarDocumentoModel($_POST['id']);
            $mensaje = ($datos['id_user'] == '') ? 'ok' : 'no';
            return $mensaje;
        }
    }
}
