<?php
date_default_timezone_set('America/Bogota');
require_once MODELO_PATH . 'torre' . DS . 'ModeloTorre.php';

class ControlTorre
{

    private static $instancia;

    public static function singleton_torre()
    {
        if (!isset(self::$instancia)) {
            $miclase         = __CLASS__;
            self::$instancia = new $miclase;
        }
        return self::$instancia;
    }

    public function buscarTorresControl($buscar)
    {
        $comando = ModeloTorre::comandoSQL();
        $mostrar = ModeloTorre::buscarTorresModel($buscar);
        return $mostrar;
    }

    public function mostrarTorresControl()
    {
        $comando = ModeloTorre::comandoSQL();
        $mostrar = ModeloTorre::mostrarTorresModel();
        return $mostrar;
    }

    public function mostrarDatosTorreControl($id)
    {
        $comando = ModeloTorre::comandoSQL();
        $mostrar = ModeloTorre::mostrarDatosTorreModel($id);
        return $mostrar;
    }

    public function mostrarDatosApartamentoControl($id)
    {
        $comando = ModeloTorre::comandoSQL();
        $mostrar = ModeloTorre::mostrarDatosApartamentoModel($id);
        return $mostrar;
    }

    public function mostrarApartamentosControl($id)
    {
        $comando = ModeloTorre::comandoSQL();
        $mostrar = ModeloTorre::mostrarApartamentosModel($id);
        return $mostrar;
    }

    public function mostrarApartamentosUsuarioControl($id)
    {
        $comando = ModeloTorre::comandoSQL();
        $mostrar = ModeloTorre::mostrarApartamentosUsuarioModel($id);
        return $mostrar;
    }

    public function agregarTorreControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log' => $_POST['id_log'],
                'nombre' => $_POST['nombre'],
            );

            $guardar = ModeloTorre::agregarTorreModel($datos);

            if ($guardar['guardar'] == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function agregarApartamentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {

            $id_user = (isset($_POST['usuario'])) ? $_POST['usuario'] : 0;

            $datos = array(
                'id_log'   => $_POST['id_log'],
                'id_torre' => $_POST['id_torre'],
                'nom_apar' => $_POST['nom_apar'],
                'id_user'  => $id_user,
            );

            $guardar = ModeloTorre::agregarApartamentoModel($datos);

            if ($guardar['guardar'] == true) {
                echo '
                <script>
                ohSnap("Guardado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("index");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }

    public function editarApartamentoControl()
    {
        if (
            $_SERVER['REQUEST_METHOD'] == 'POST' &&
            isset($_POST['id_log']) &&
            !empty($_POST['id_log'])
        ) {
            $datos = array(
                'id_log'         => $_POST['id_log'],
                'id_apartamento' => $_POST['id_apartamento'],
                'nombre'         => $_POST['nombre'],
                'id_user'        => $_POST['id_usuario'],
                'fecha_update'   => date('Y-m-d H:i:s'),
            );

            $guardar = ModeloTorre::editarApartamentoModel($datos);

            if ($guardar == true) {
                echo '
                <script>
                ohSnap("Actualizado correctamente!", {color: "green", "duration": "1000"});
                setTimeout(recargarPagina,1050);

                function recargarPagina(){
                    window.location.replace("listado?torre=' . base64_encode($_POST['id_torre']) . '");
                }
                </script>
                ';
            } else {
                echo '
                <script>
                ohSnap("Ha ocurrido un error!", {color: "red"});
                </script>
                ';
            }
        }
    }
}
