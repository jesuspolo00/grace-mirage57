<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloTorre extends conexion
{

    public static function agregarTorreModel($datos)
    {
        $tabla  = 'torres';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, user_log) VALUES (:n, :idl)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':n', $datos['nombre']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('id' => $id, 'guardar' => true);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarTorresModel()
    {
        $tabla  = 'torres';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        t.*,
        (SELECT COUNT(a.id) FROM apartamentos a WHERE a.id_torre = t.id) AS cantidad
        FROM " . $tabla . " t;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarTorresModel($buscar)
    {
        $tabla  = 'torres';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        t.*,
        (SELECT COUNT(a.id) FROM apartamentos a WHERE a.id_torre = t.id) AS cantidad
        FROM " . $tabla . " t WHERE t.nombre LIKE '%" . $buscar . "%'";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosTorreModel($id)
    {
        $tabla  = 'torres';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarApartamentosModel($id)
    {
        $tabla  = 'apartamentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        a.*,
        u.*,
        a.nombre as nom_apar,
        u.nombre AS nom_user,
        CONCAT(u.nombre, ' ', u.apellido) as usuario_asignado,
        a.activo AS activo_apartamento
        FROM apartamentos a
        INNER JOIN usuarios u ON u.id_user = a.id_user
        WHERE a.id_torre = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosApartamentoModel($id)
    {
        $tabla  = 'apartamentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        a.*,
        u.*,
        a.nombre as nom_apar,
        u.nombre AS nom_user,
        CONCAT(u.nombre, ' ', u.apellido) as usuario_asignado
        FROM apartamentos a
        INNER JOIN usuarios u ON u.id_user = a.id_user
        WHERE a.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarApartamentosUsuarioModel($id)
    {
        $tabla  = 'apartamentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        a.*,
        (SELECT t.nombre FROM torres t WHERE t.id = a.id_torre) AS nom_torre
        FROM apartamentos a WHERE a.id_user = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function agregarApartamentoModel($datos)
    {
        $tabla  = 'apartamentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, id_torre, id_log, id_user) VALUES (:n, :idt, :idl, :idu)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nom_apar']);
            $preparado->bindParam(':idt', $datos['id_torre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':idu', $datos['id_user']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('id' => $id, 'guardar' => true);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarApartamentoModel($datos)
    {
        $tabla  = 'apartamentos';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET id_user = :idu, id_log = :idl, nombre = :n, fecha_update = :fu WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':fu', $datos['fecha_update']);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':idu', $datos['id_user']);
            $preparado->bindParam(':id', $datos['id_apartamento']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
