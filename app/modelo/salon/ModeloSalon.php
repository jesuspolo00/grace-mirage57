<?php
require_once MODELO_PATH . 'conexion.php';

class ModeloSalon extends conexion
{

    public static function guardarSalonModel($datos)
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (nombre, horas, dias, cantidad, id_user) VALUES (:n, :h, :d, :c, :idu)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':h', $datos['horas']);
            $preparado->bindParam(':d', $datos['dias']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':idu', $datos['id_log']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('id' => $id, 'guardar' => true);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function guardarHorasPicoModel($datos)
    {
        $tabla  = 'horas_pico';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_salon, id_hora, id_user) VALUES (:ids, :idh, :idl);
        UPDATE salones SET dias = 1 WHERE id = :ids;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['id_salon']);
            $preparado->bindParam(':idh', $datos['hora']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function editarSalonModel($datos)
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET nombre = :n, horas = :h, dias = :d, cantidad = :c, id_user = :idu WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':n', $datos['nombre']);
            $preparado->bindParam(':h', $datos['hora']);
            $preparado->bindParam(':d', $datos['dias']);
            $preparado->bindParam(':c', $datos['cantidad']);
            $preparado->bindParam(':idu', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_salon']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSalonesModel()
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla . " WHERE activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosSalonesModel($id)
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla . " WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSalonesReservaModel()
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE s.* FROM salones s WHERE s.id IN(SELECT r.id_salon FROM reservas r WHERE r.activo = 1 AND r.fecha_reserva = '" . date('Y-m-d') . "');";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSalonesUsuarioReservaModel($id)
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE s.* FROM salones s WHERE s.id IN(SELECT r.id_salon FROM reservas r WHERE r.activo = 1 AND r.fecha_reserva = '" . date('Y-m-d') . "' AND r.id_user = :id);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarSalonesIdReservaModel($datos)
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        s.*
        FROM salones s
        LEFT JOIN reservas r ON r.id_salon = s.id
        LEFT JOIN usuarios u ON u.id_user = r.id_user
        WHERE CONCAT(u.nombre, ' ', u.apellido, ' ', s.nombre, ' ', r.fecha_reserva) LIKE '%" . $datos['user'] . "%' " . $datos['salon'] . " " . $datos['fecha'] . " AND r.activo = 1 GROUP BY s.id ORDER BY r.fecha_reserva DESC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHorasModel()
    {
        $tabla  = 'horas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla;
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHorasSalonModel($id)
    {
        $tabla  = 'horas_pico';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        hr.*,
        h.horas
        FROM horas_pico hr
        LEFT JOIN horas h ON h.id = hr.id_hora
        WHERE hr.id_salon = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarHoraPicoSalonModel($id, $salon)
    {
        $tabla  = 'horas_pico';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        hr.*
        FROM horas_pico hr
        WHERE hr.id_salon = :id AND hr.id_hora = :h AND hr.activo = 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':h', $id);
            $preparado->bindParam(':id', $salon);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function desactivarHorasPicoSalonModel($datos)
    {
        $tabla  = 'horas_pico';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id_salon = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id_salon']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosDetalleReservaModel()
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        r.id as id_reserva,
        u.id_user AS id_user,
        CONCAT(u.nombre , ' ', u.apellido) AS nom_user,
        s.id AS id_salon,
        s.nombre AS salon,
        ap.id AS id_apa,
        ap.nombre AS nom_apa,
        r.fecha_reserva,
        (SELECT h.horas FROM horas h WHERE h.id = d.hora_reserva) AS hora,
        r.detalle_reserva,
        r.activo,
        d.id as id_detalle_reserva,
        d.activo as detalle_activo
        FROM salones s
        LEFT JOIN reservas r ON r.id_salon = s.id
        LEFT JOIN detalle_reserva d ON d.id_reserva = r.id
        LEFT JOIN apartamentos ap ON ap.id = r.id_apartamento
        LEFT JOIN usuarios u ON u.id_user = r.id_user
        WHERE d.id_salon = s.id and r.fecha_reserva = '" . date('Y-m-d') . "' ORDER BY r.fecha_reserva, d.hora_reserva;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosDetalleReservaUsuarioModel($id)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        r.id as id_reserva,
        u.id_user AS id_user,
        CONCAT(u.nombre , ' ', u.apellido) AS nom_user,
        s.id AS id_salon,
        s.nombre AS salon,
        ap.id AS id_apa,
        ap.nombre AS nom_apa,
        r.fecha_reserva,
        (SELECT h.horas FROM horas h WHERE h.id = d.hora_reserva) AS hora,
        r.detalle_reserva,
        r.activo,
        d.id as id_detalle_reserva,
        d.activo as detalle_activo
        FROM salones s
        LEFT JOIN reservas r ON r.id_salon = s.id
        LEFT JOIN detalle_reserva d ON d.id_reserva = r.id
        LEFT JOIN apartamentos ap ON ap.id = r.id_apartamento
        LEFT JOIN usuarios u ON u.id_user = r.id_user
        WHERE d.id_salon = s.id AND r.id_user = :id  and r.fecha_reserva = '" . date('Y-m-d') . "' ORDER BY r.fecha_reserva, d.hora_reserva LIMIT 50;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosIdReservaModel($id)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT * FROM " . $tabla . " WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosIdDetalleReservaModel($id)
    {
        $tabla  = 'detalle_reserva';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        d.*,
        h.horas
        FROM detalle_reserva d
        LEFT JOIN horas h ON h.id = d.hora_reserva
        WHERE d.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarDatosDetalleReservaModel($datos)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        r.id as id_reserva,
        u.id_user AS id_user,
        CONCAT(u.nombre , ' ', u.apellido) AS nom_user,
        s.id AS id_salon,
        s.nombre AS salon,
        ap.id AS id_apa,
        ap.nombre AS nom_apa,
        r.fecha_reserva,
        (SELECT h.horas FROM horas h WHERE h.id = d.hora_reserva) AS hora,
        r.detalle_reserva,
        r.activo,
        d.id as id_detalle_reserva,
        d.activo as detalle_activo
        FROM salones s
        LEFT JOIN reservas r ON r.id_salon = s.id
        LEFT JOIN detalle_reserva d ON d.id_reserva = r.id
        LEFT JOIN apartamentos ap ON ap.id = r.id_apartamento
        LEFT JOIN usuarios u ON u.id_user = r.id_user
        WHERE CONCAT(u.nombre , ' ', u.apellido, ' ', s.nombre, ' ', ap.nombre, ' ', r.fecha_reserva) LIKE '%" . $datos['user'] . "%'
        " . $datos['salon'] . "
        " . $datos['fecha'] . "
        ORDER BY r.fecha_reserva, d.hora_reserva;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function buscarDatosDetalleReservaUsuarioModel($datos)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
        r.id as id_reserva,
        u.id_user AS id_user,
        CONCAT(u.nombre , ' ', u.apellido) AS nom_user,
        s.id AS id_salon,
        s.nombre AS salon,
        ap.id AS id_apa,
        ap.nombre AS nom_apa,
        r.fecha_reserva,
        (SELECT h.horas FROM horas h WHERE h.id = d.hora_reserva) AS hora,
        r.detalle_reserva,
        r.activo,
        d.id as id_detalle_reserva,
        d.activo as detalle_activo
        FROM salones s
        LEFT JOIN reservas r ON r.id_salon = s.id
        LEFT JOIN detalle_reserva d ON d.id_reserva = r.id
        LEFT JOIN apartamentos ap ON ap.id = r.id_apartamento
        LEFT JOIN usuarios u ON u.id_user = r.id_user
        WHERE CONCAT(u.nombre , ' ', u.apellido, ' ', s.nombre, ' ', ap.nombre, ' ', r.fecha_reserva) LIKE '%" . $datos['user'] . "%'
        " . $datos['salon'] . "
        " . $datos['fecha'] . "
        AND r.id_user = :id ORDER BY r.fecha_reserva, d.hora_reserva;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['id']);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosSalonIdModel($id)
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
        s.*,
        (SELECT hr.id FROM horas_pico hr WHERE hr.id_salon = s.id ORDER BY hr.id DESC LIMIT 1) AS hora_pico
        FROM salones s
        WHERE s.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reservarSalonModel($datos)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_salon, id_apartamento, fecha_reserva, detalle_reserva, id_user) VALUES (:ids, :idp, :fr, :dr, :idl);";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['salon']);
            $preparado->bindParam(':idp', $datos['apartamento']);
            $preparado->bindParam(':fr', $datos['fecha']);
            $preparado->bindParam(':dr', $datos['detalle_reserva']);
            $preparado->bindParam(':idl', $datos['id_log']);
            if ($preparado->execute()) {
                $id        = $cnx->ultimoIngreso($tabla);
                $resultado = array('guardar' => true, 'id' => $id);
                return $resultado;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reservarPortatilModel($salon, $fecha)
    {
        $tabla  = 'portatil';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM portatil p WHERE p.id NOT IN(SELECT r.portatil FROM reservas r WHERE r.id_salon = " . $salon . " AND r.fecha_reserva = '" . $fecha . "') ORDER BY p.id DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarDatosReservaModel($id)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
            SQL_NO_CACHE
            r.*,
            (SELECT h.horas FROM horas h WHERE h.id = r.hora_reserva) as hora_numero,
            (SELECT s.nombre FROM salones s WHERE s.id = r.id_salon) AS salon
            FROM reservas r WHERE r.id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHorasIdModel($id)
    {
        $tabla  = 'horas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE * FROM " . $tabla . " WHERE id = :id";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHorasDisponiblesModel($datos)
    {
        $tabla  = 'horas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE
            h.*
            FROM horas h WHERE h.id NOT IN(SELECT d.hora_reserva FROM detalle_reserva d
                WHERE d.activo = 1 AND d.id_reserva IN(SELECT r.id FROM reservas r WHERE r.id_salon = :ids AND r.fecha_reserva = '" . $datos['fecha'] . "'))
                    ORDER BY h.horas ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['salon']);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function noMostrarHorasPicoDisponiblesModel($datos)
    {
        $tabla  = 'horas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                    h.*
                    FROM horas h
                    WHERE h.id IN(SELECT hr.id_hora FROM horas_pico hr WHERE hr.id_salon = :ids AND hr.activo = 1)
                    AND h.id NOT IN(SELECT dt.hora_reserva FROM detalle_reserva dt WHERE dt.activo = 1 AND dt.id_reserva IN(
                        SELECT r.id FROM reservas r WHERE r.fecha_reserva = '" . $datos['fecha'] . "' AND r.id_salon = :ids AND r.activo = 1)) ORDER BY h.horas ASC;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['salon']);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarSemanaModel($datos)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        SQL_NO_CACHE
                        r.id,
                        r.id_apartamento,
                        r.id_salon,
                        COUNT(r.fecha_reserva) AS cantidad_reservado
                        FROM " . $tabla . " r WHERE r.id_apartamento = :id AND r.id_salon = :s AND r.fecha_reserva >= '" . $datos['inicio_semana'] . "' AND r.fecha_reserva <= '" . $datos['fin_semana'] . "' AND r.activo = 1 GROUP BY r.id_salon;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['apartamento']);
            $preparado->bindParam(':s', $datos['salon']);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function validarHorasModel($datos)
    {
        $tabla  = 'detalle_reserva';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                        d.*,
                        COUNT(d.hora_reserva) AS cantidad_horas
                        FROM detalle_reserva d WHERE d.id_reserva IN(SELECT r.id FROM reservas r WHERE r.fecha_reserva = '" . $datos['fecha'] . "') GROUP BY d.id_reserva;
                            ";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function ultimaReservaSalonModel($datos)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT SQL_NO_CACHE r.* FROM " . $tabla . " r
                            WHERE r.id_apartamento = :id AND r.id_salon = :ids ORDER BY r.fecha_reserva DESC LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $datos['apartamento']);
            $preparado->bindParam(':ids', $datos['salon']);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function horasRestantesSalonModel($datos)
    {
        $tabla  = 'salones';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                            s.*,
                            s.horas - (SELECT
                                COUNT(d.hora_reserva)
                                FROM detalle_reserva d WHERE d.id_reserva IN(SELECT r.id FROM reservas r WHERE r.fecha_reserva = '" . $datos['fecha'] . "' AND r.id_salon = s.id AND r.id_apartamento = :id AND r.activo = 1) GROUP BY d.id_reserva)AS horas_restantes
                                    FROM " . $tabla . " s WHERE s.id = :ids;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ids', $datos['salon']);
            $preparado->bindParam(':id', $datos['apartamento']);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reservarHorasModel($datos)
    {
        $tabla  = 'detalle_reserva';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "INSERT INTO " . $tabla . " (id_reserva, id_apartamento, id_salon, hora_reserva) VALUES (:idr, :ida, :ids, :h)";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':idr', $datos['id_reserva']);
            $preparado->bindParam(':ida', $datos['id_apartamento']);
            $preparado->bindParam(':ids', $datos['id_salon']);
            $preparado->bindParam(':h', $datos['horas']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cancelarReservaModel($datos)
    {
        $tabla  = 'detalle_reserva';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = " UPDATE " . $tabla . " SET activo = 0, motivo_cancelacion = :m, fecha_cancelacion = :fc, id_log = :idl WHERE id = :idr AND id_reserva = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':m', $datos['motivo']);
            $preparado->bindParam(':fc', $datos['fecha']);
            $preparado->bindParam(':idl', $datos['id_log']);
            $preparado->bindParam(':id', $datos['id_reserva']);
            $preparado->bindParam(':idr', $datos['id_detalle_reserva']);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function contarDetalleActivoModel($id)
    {
        $tabla  = 'detalle_reserva';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                                    COUNT(id) as contar
                                    FROM detalle_reserva
                                    WHERE activo = 1 AND id_reserva = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function cancelarReservaTotalModel($id)
    {
        $tabla  = 'reservas';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "UPDATE " . $tabla . " SET activo = 0 WHERE id = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function reservaSalonFechaAnteriorModel($datos, $fecha)
    {
        $tabla  = 'detalle_reserva';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                                    d.id
                                    FROM detalle_reserva d
                                    WHERE d.id_reserva IN(SELECT r.id FROM reservas r WHERE r.id_salon = :ids AND r.id_apartamento = :ida AND r.fecha_reserva = '" . $fecha . "')
                                        AND d.hora_reserva IN(SELECT hr.id_hora FROM horas_pico hr WHERE hr.id_salon = :ids) AND d.activo = 1 LIMIT 1;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':ida', $datos['apartamento']);
            $preparado->bindParam(':ids', $datos['salon']);
            if ($preparado->execute()) {
                return $preparado->fetch();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function mostrarHorasReservadasModel($id)
    {
        $tabla  = 'detalle_reserva';
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SELECT
                                        h.horas
                                        FROM detalle_reserva d
                                        LEFT JOIN horas h ON h.id = d.hora_reserva
                                        WHERE d.id_reserva = :id;";
        try {
            $preparado = $cnx->preparar($cmdsql);
            $preparado->bindParam(':id', $id);
            if ($preparado->execute()) {
                return $preparado->fetchAll();
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }

    public static function comandoSQL()
    {
        $cnx    = conexion::singleton_conexion();
        $cmdsql = "SET SQL_BIG_SELECTS=1";
        try {
            $preparado = $cnx->preparar($cmdsql);
            if ($preparado->execute()) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage();
        }
        $cnx->closed();
        $cnx = null;
    }
}
