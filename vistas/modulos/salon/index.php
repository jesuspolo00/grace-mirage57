<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'salon' . DS . 'ControlSalones.php';

$instancia = ControlSalon::singleton_salon();

$datos_salon = $instancia->mostrarSalonesControl();
$datos_horas = $instancia->mostrarHorasControl();

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 4);

if (!$permisos) {
    include_once VISTA_PATH . DS . 'modulos' . DS . '403.php';
    exit();
}
?>
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <div class="card shadow-sm mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                    <h4 class="m-0 font-weight-bold text-haj">
                        <a href="<?=BASE_URL?>inicio" class="text-decoration-none">
                            <i class="fa fa-arrow-left text-haj"></i>
                        </a>
                        &nbsp;
                        Salones
                    </h4>
                    <div class="dropdown no-arrow">
                        <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
                            <div class="dropdown-header">Acciones:</div>
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_salon">Agregar Salon</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-8 form-inline">
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <input type="text" class="form-control filtro" placeholder="Buscar">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text rounded-right" id="basic-addon1">
                                            <i class="fa fa-search"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-2">
                        <table class="table table-hover border table-sm" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center font-weight-bold">
                                    <th scope="col">No. Salon</th>
                                    <th scope="col">Nombre</th>
                                    <th scope="col">Horas reservables (DIA)</th>
                                    <th scope="col">Frecuencia reserva (DIAS)</th>
                                    <th scope="col">Cantida reserva (SEMANA)</th>
                                    <th scope="col">Horas pico</th>
                                </tr>
                            </thead>
                            <tbody class="buscar">
                                <?php
                                foreach ($datos_salon as $salon) {
                                    $id_salon = $salon['id'];
                                    $nombre   = $salon['nombre'];
                                    $horas    = $salon['horas'];
                                    $dias     = $salon['dias'];
                                    $cantidad = $salon['cantidad'];

                                    $horas_salon = $instancia->mostrarHorasSalonControl($id_salon);
                                    $horas_texto = '';
                                    foreach ($horas_salon as $hora) {
                                        $activo = $hora['activo'];
                                        $horas_texto .= ($activo == 0) ? '' : $hora['horas'] . ' || ';
                                    }
                                    $horas_texto .= '';
                                    ?>
                                    <tr class="text-center text-uppercase">
                                        <td><?=$id_salon?></td>
                                        <td><?=$nombre?></td>
                                        <td><?=($horas / 2)?></td>
                                        <td><?=$dias?></td>
                                        <td><?=$cantidad?></td>
                                        <td><?=$horas_texto?></td>
                                        <td>
                                            <div class="btn-group btn-group-sm" role="group">
                                                <button class="btn btn-haj btn-sm" data-tooltip="tooltip" title="Editar" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#editar<?=$id_salon?>">
                                                    <i class="fa fa-edit"></i>
                                                </button>
                                                <!-- <button class="btn btn-danger btn-sm" data-tooltip="tooltip" title="Desactivar" data-placement="bottom" data-trigger="hover">
                                                    <i class="fa fa-times"></i>
                                                </button> -->
                                            </div>
                                        </td>
                                    </tr>


                                    <!-- Modal -->
                                    <div class="modal fade" id="editar<?=$id_salon?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-lg" role="document">
                                            <div class="modal-content">
                                              <div class="modal-header">
                                                <h5 class="modal-title font-weight-bold text-haj" id="exampleModalLabel">Editar salon</h5>
                                            </div>
                                            <div class="modal-body">
                                                <form method="POST">
                                                    <input type="hidden" name="id_log" value="<?=$id_log?>">
                                                    <input type="hidden" name="id_salon" value="<?=$id_salon?>">
                                                    <div class="row p-2">
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Salon <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control" value="<?=$nombre?>" name="nombre_edit" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Horas reservables <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control numeros" value="<?=($horas / 2)?>" name="hora_edit" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Frecuencia de reserva (dias) <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control numeros" value="<?=$dias?>" name="dias_edit" required>
                                                        </div>
                                                        <div class="col-lg-6 form-group">
                                                            <label class="font-weight-bold">Cantidad de reservas (semana) <span class="text-danger">*</span></label>
                                                            <input type="text" class="form-control numeros" value="<?=$cantidad?>" name="cantidad_edit" required>
                                                        </div>
                                                        <div class="col-lg-12 form-group">
                                                          <label class="font-weight-bold">Horas pico <span class="text-danger">*</span></label>
                                                          <div class="form-inline">
                                                            <?php
                                                            foreach ($datos_horas as $hora) {
                                                                $id_hora  = $hora['id'];
                                                                $nom_hora = $hora['horas'];

                                                                $buscar_hora = $instancia->buscarHoraPicoSalonControl($id_hora, $id_salon);
                                                                $check       = ($buscar_hora['id'] == '') ? '' : 'checked';
                                                                ?>
                                                                <div class="custom-control custom-switch ml-4">
                                                                    <input type="checkbox" class="custom-control-input hora_check" id="<?=$id_salon?><?=$id_hora?>" name="horas_pico_edit[]" value="<?=$id_hora?>" <?=$check?>>
                                                                    <label class="custom-control-label" for="<?=$id_salon?><?=$id_hora?>"><?=$nom_hora?></label>
                                                                </div>
                                                            <?php }?>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 form-group mt-2 text-right">
                                                        <button class="btn btn-danger btn-sm" data-dismiss="modal">
                                                            <i class="fa fa-times"></i>
                                                            &nbsp;
                                                            Cancelar
                                                        </button>
                                                        <button class="btn btn-haj btn-sm" type="submit">
                                                            <i class="fa fa-save"></i>
                                                            &nbsp;
                                                            Guardar
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'salon' . DS . 'agregarSalon.php';

if (isset($_POST['nombre'])) {
    $instancia->guardarSalonControl();
}

if (isset($_POST['nombre_edit'])) {
    $instancia->editarSalonControl();
}
?>