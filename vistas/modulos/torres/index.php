<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'torre' . DS . 'ControlTorre.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia         = ControlTorre::singleton_torre();
$instancia_usuario = ControlUsuarios::singleton_usuario();

$datos_usuarios = $instancia_usuario->mostrarTodosUsuariosControl();

if (isset($_POST['buscar'])) {
	$datos_torre = $instancia->buscarTorresControl($_POST['buscar']);
} else {
	$datos_torre = $instancia->mostrarTorresControl();
}

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 3);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-haj">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none text-haj">
							<i class="fa fa-arrow-left"></i>
						</a>
						&nbsp;
						Torres
					</h4>
					<div class="dropdown no-arrow">
						<a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
						</a>
						<div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(17px, 19px, 0px);">
							<div class="dropdown-header">Acciones:</div>
							<a class="dropdown-item" href="#" data-toggle="modal" data-target="#agregar_torre">Agregar torres</a>
						</div>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8"></div>
							<div class="form-group col-lg-4">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar" name="buscar" aria-describedby="basic-addon2" data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-haj btn-sm" type="submit" data-tooltip="tooltip" title="CLIC para buscar" data-placement="top" data-trigger="hover">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<tr class="text-center font-weight-bold">
								<th scope="col">Torre</th>
								<th scope="col">Apartamentos - Cantidad</th>
							</tr>
							<tbody class="buscar">
								<?php
								foreach ($datos_torre as $torre) {
									$id_torre = $torre['id'];
									$nombre   = $torre['nombre'];
									$cantidad = $torre['cantidad'];
									?>
									<tr class="text-center text-uppercase">
										<td><?=$nombre?></td>
										<td><?=$cantidad?></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-haj btn-sm" type="button" data-tooltip="tooltip" title="Agregar Apartamentos" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#apartamento<?=$id_torre?>">
													<i class="fa fa-plus"></i>
												</button>
												<a href="<?=BASE_URL?>torres/listado?torre=<?=base64_encode($id_torre)?>" class="btn btn-info btn-sm" data-tooltip="tooltip" title="Ver apartamentos" data-placement="bottom" data-trigger="hover">
													<i class="fa fa-eye"></i>
												</a>
											</div>
										</td>
									</tr>

									<!--Agregar usuario-->
									<div class="modal fade" id="apartamento<?=$id_torre?>" tabindex="-1" role="modal" aria-hidden="true" aria-labelledby="exampleModalLabel">
										<div class="modal-dialog modal-lg p-2" role="document">
											<div class="modal-content">
												<form method="POST">
													<input type="hidden" value="<?=$id_log?>" name="id_log">
													<input type="hidden" value="<?=$id_torre?>" name="id_torre">
													<div class="modal-header p-3">
														<h4 class="modal-title text-haj font-weight-bold">Agregar Apartamento</h4>
													</div>
													<div class="modal-body border-0">
														<div class="row  p-3">
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
																<input type="text" class="form-control" disabled value="<?=$nombre?>">
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Apartamento <span class="text-danger">*</span></label>
																<input type="text" name="nom_apar" class="form-control" required>
															</div>
															<div class="col-lg-6 form-group">
																<label class="font-weight-bold">Usuario a asignar (opcional)</label>
																<select class="form-control select2" data-live-search="true" name="usuario">
																	<option selected="" value="0">Seleccione una opcion...</option>
																	<?php
																	foreach ($datos_usuarios as $usuario) {
																		$id_user  = $usuario['id_user'];
																		$nom_user = $usuario['nombre'] . ' ' . $usuario['apellido'];
																		$activo   = $usuario['activo'];

																		$ver = ($activo == 0 || $usuario['perfil'] == 1) ? 'd-none' : '';
																		?>
																		<option value="<?=$id_user?>" class="<?=$ver?>"><?=$nom_user?></option>
																	<?php }?>
																</select>
															</div>
															<div class="col-lg-12 form-group text-right mt-2">
																<button class="btn btn-danger btn-sm" data-dismiss="modal">
																	<i class="fa fa-times"></i>
																	&nbsp;
																	Cancelar
																</button>
																<button type="submit" class="btn btn-haj btn-sm">
																	<i class="fa fa-save"></i>
																	&nbsp;
																	Registrar
																</button>
															</div>
														</div>
													</div>
												</form>
											</div>
										</div>
									</div>


								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'torres' . DS . 'agregarTorre.php';

if (isset($_POST['nombre'])) {
	$instancia->agregarTorreControl();
}

if (isset($_POST['nom_apar'])) {
	$instancia->agregarApartamentoControl();
}
?>
