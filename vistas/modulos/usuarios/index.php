<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
	$er    = '2';
	$error = base64_encode($er);
	$salir = new Session;
	$salir->iniciar();
	$salir->outsession();
	header('Location:../login?er=' . $error);
	exit();
}
include_once VISTA_PATH . 'cabeza.php';
include_once VISTA_PATH . 'navegacion.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'usuarios' . DS . 'ControlUsuarios.php';

$instancia        = ControlUsuarios::singleton_usuario();
$instancia_perfil = ControlPerfil::singleton_perfil();

$datos_perfil = $instancia_perfil->mostrarPerfilesControl();

if (isset($_POST['buscar'])) {
	$datos_usuarios = $instancia->mostrarUsuariosBuscarControl($_POST['buscar']);
} else {
	$datos_usuarios = $instancia->mostrarUsuariosControl();
}

$permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 2);
if (!$permisos) {
	include_once VISTA_PATH . 'modulos' . DS . '403.php';
	die();
}
?>
<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<div class="card shadow-sm mb-4">
				<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
					<h4 class="m-0 font-weight-bold text-haj">
						<a href="<?=BASE_URL?>inicio" class="text-decoration-none text-haj">
							<i class="fa fa-arrow-left"></i>
						</a>
						&nbsp;
						Usuarios
					</h4>
					<div class="btn-group">
						<button class="btn btn-haj btn-sm" type="button" data-toggle="modal" data-target="#agregar_usuario">
							<i class="fa fa-plus"></i>
							&nbsp;
							Agregar Usuario
						</button>
					</div>
				</div>
				<div class="card-body">
					<form method="POST">
						<div class="row">
							<div class="col-lg-8"></div>
							<div class="form-group col-lg-4">
								<div class="input-group mb-3">
									<input type="text" class="form-control filtro" placeholder="Buscar" name="buscar" aria-describedby="basic-addon2" data-tooltip="tooltip" title="Presione ENTER para buscar" data-placement="top" data-trigger="focus">
									<div class="input-group-append">
										<button class="btn btn-haj btn-sm" type="submit" data-tooltip="tooltip" title="CLIC para buscar" data-placement="top" data-trigger="hover">
											<i class="fa fa-search"></i>
											&nbsp;
											Buscar
										</button>
									</div>
								</div>
							</div>
						</div>
					</form>
					<div class="table-responsive mt-2">
						<table class="table table-hover border table-sm" width="100%" cellspacing="0">
							<tr class="text-center font-weight-bold">
								<th scope="col">Documento</th>
								<th scope="col">Nombre Completo</th>
								<th scope="col">Correo</th>
								<th scope="col">Telefono</th>
								<th scope="col">Usuario</th>
								<th scope="col">Perfil</th>
							</tr>
							<tbody class="buscar">
								<?php
								foreach ($datos_usuarios as $usuario) {
									$id_user      = $usuario['id_user'];
									$documento    = $usuario['documento'];
									$nom_completo = $usuario['nombre'] . ' ' . $usuario['apellido'];
									$correo       = $usuario['correo'];
									$telefono     = $usuario['telefono'];
									$user_nom     = $usuario['user'];
									$perfil       = $usuario['nom_perfil'];
									$activo       = $usuario['activo'];

									$icon  = ($activo == 1) ? '<i class="fa fa-times"></i>' : '<i class="fa fa-check"></i>';
									$color = ($activo == 1) ? 'btn-danger' : 'btn-success';
									$title = ($activo == 1) ? 'Inactivar' : 'Activar';
									$class = ($activo == 1) ? 'inactivar' : 'activar';

									if ($perfil != 1) {
										?>
										<tr class="text-center user_<?=$id_user?>">
											<td><?=$documento?></td>
											<td><?=$nom_completo?></td>
											<td><?=$correo?></td>
											<td><?=$telefono?></td>
											<td><?=$user_nom?></td>
											<td><?=$perfil?></td>
											<td>
												<div class="btn-group" role="group">
													<button class="btn btn-haj btn-sm" data-tooltip="tooltip" title="Editar" data-placement="bottom" data-trigger="hover" data-toggle="modal" data-target="#editar<?=$id_user?>">
														<i class="fa fa-user-edit"></i>
													</button>
													<button class="btn <?=$color?> btn-sm <?=$class?> user<?=$id_user?>" data-tooltip="tooltip" title="<?=$title?>" data-placement="bottom" data-trigger="hover" id="<?=$id_user?>">
														<?=$icon?>
													</button>
												</div>
											</td>
										</tr>


										<!-- Modal -->
										<div class="modal fade" id="editar<?=$id_user?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h5 class="modal-title font-weight-bold text-haj">Editar Usuario</h5>
													</div>
													<div class="modal-body">
														<form method="POST">
															<input type="hidden" name="id_user" value="<?=$id_user?>">
															<input type="hidden" name="id_log" value="<?=$id_log?>">
															<div class="row p-2">
																<div class="form-group col-lg-6">
																	<label class="font-weight-bold">Documento <span class="text-danger">*</span></label>
																	<input type="text" class="form-control numeros" required value="<?=$documento?>" name="documento_edit" required>
																</div>
																<div class="form-group col-lg-6">
																	<label class="font-weight-bold">Nombre <span class="text-danger">*</span></label>
																	<input type="text" class="form-control" required value="<?=$usuario['nombre']?>" name="nombre_edit" required>
																</div>
																<div class="form-group col-lg-6">
																	<label class="font-weight-bold">Apellido <span class="text-danger">*</span></label>
																	<input type="text" class="form-control" required value="<?=$usuario['apellido']?>" name="apellido_edit">
																</div>
																<div class="form-group col-lg-6">
																	<label class="font-weight-bold">Correo <span class="text-danger">*</span></label>
																	<input type="email" class="form-control" required value="<?=$correo?>" name="correo_edit" required>
																</div>
																<div class="form-group col-lg-6">
																	<label class="font-weight-bold">Telefono </label>
																	<input type="text" class="form-control numeros" value="<?=$telefono?>" name="telefono_edit">
																</div>
																<div class="form-group col-lg-6">
																	<label class="font-weight-bold">Usuario <span class="text-danger">*</span></label>
																	<input type="text" class="form-control" value="<?=$user_nom?>" disabled>
																</div>
																<div class="col-lg-6">
																	<label class="font-weight-bold">Perfil <span class="text-danger">*</span></label>
																	<select class="form-control" name="perfil_edit" required>
																		<option value="<?=$usuario['perfil']?>" selected class="d-none"><?=$perfil?></option>
																		<?php
																		foreach ($datos_perfil as $perfiles) {
																			$id_perfil = $perfiles['id'];
																			$nombre    = $perfiles['nombre'];
																			$activo    = $perfiles['activo'];

																			$ver = ($activo == 0 || $id_perfil == 1) ? 'd-none' : '';
																			?>
																			<option value="<?=$id_perfil?>" class="<?=$ver?>"><?=$nombre?></option>
																			<?php
																		}
																		?>
																	</select>
																</div>
																<div class="col-lg-12 mt-4">
																	<div class="row">
																		<div class="col-lg-6 form-group text-left">
																			<button class="btn btn-haj btn-sm restablecer" data-doc="<?=$documento?>" type="button" id="<?=$id_user?>" data-toggle="popover" data-placement="left" data-trigger="hover" title="Restablecer Contrase&ntilde;a" data-content="Se restablecera la contraseña al ultimo numero de documento guardado.">
																				<i class="fas fa-sync-alt"></i>
																				&nbsp;
																				Restablecer Contrase&ntilde;a
																			</button>
																		</div>
																		<div class="col-lg-6 form-group text-right">

																			<button class="btn btn-danger btn-sm text-right" data-dismiss="modal">
																				<i class="fa fa-times"></i>
																				&nbsp;
																				Cancelar
																			</button>
																			<button class="btn btn-success btn-sm text-right" type="submit">
																				<i class="fa fa-save"></i>
																				&nbsp;
																				Guardar
																			</button>
																		</div>
																	</div>
																</div>
															</div>
														</form>
													</div>
												</div>
											</div>
										</div>
										<?php
									}
								}
								?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
include_once VISTA_PATH . 'script_and_final.php';
include_once VISTA_PATH . 'modulos' . DS . 'usuarios' . DS . 'agregarUsuario.php';

if (isset($_POST['documento'])) {
	$instancia->agregarUsuarioControl();
}

if (isset($_POST['documento_edit'])) {
	$instancia->editarUsuarioControl();
}
?>
<script src="<?=PUBLIC_PATH?>js/usuarios/funcionesUsuarios.js"></script>