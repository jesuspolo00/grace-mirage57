<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
    $er    = '2';
    $error = base64_encode($er);
    $salir = new Session;
    $salir->iniciar();
    $salir->outsession();
    header('Location:../login?er=' . $error);
    exit();
}
require_once LIB_PATH . 'tcpdf' . DS . 'tcpdf.php';
require_once CONTROL_PATH . 'perfil' . DS . 'ControlPerfil.php';
require_once CONTROL_PATH . 'salon' . DS . 'ControlSalones.php';

$instancia        = ControlSalon::singleton_salon();
$instancia_perfil = ControlPerfil::singleton_perfil();

if (isset($_GET['salon'])) {

    $buscar_salon = base64_decode($_GET['salon']);
    $buscar_fecha = base64_decode($_GET['fecha']);
    $buscar_user  = base64_decode($_GET['user']);

    $datos = array('salon' => $buscar_salon, 'fecha' => $buscar_fecha, 'user' => $buscar_user);

    $datos_salones_reserva = $instancia->mostrarSalonesReservaControl();
    $datos_reserva         = $instancia->buscarDatosDetalleReservaControl($datos);

    class MYPDF extends TCPDF
    {
        public function setData($logo)
        {
            $this->logo = $logo;
        }

        public function Header()
        {
        }

        public function Footer()
        {
            $this->SetY(-15);
            $this->SetFillColor(127);
            $this->SetTextColor(127);
            $this->SetFont(PDF_FONT_NAME_MAIN, 'I', 10);
            $this->Cell(0, 10, 'Pagina ' . $this->PageNo(), 0, 0, 'C');
        }
    }

    $pdf = new MYPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

    $pdf->SetCreator(PDF_CREATOR);
    //$pdf->setData($datos_super_empresa['imagen']);
    $pdf->SetAuthor('Jesus Polo');
    $pdf->SetTitle('Programacion Diaria');
    $pdf->SetSubject('Programacion Diaria');
    $pdf->SetKeywords('Programacion Diaria');
    $pdf->setPageOrientation('L');
    $pdf->AddPage();

    $pdf->setJPEGQuality(90);
    //$pdf->Image(PUBLIC_PATH . 'img/' ., 120, 5, 50);

    $pdf->Ln(1);
    $pdf->Cell(131);
    $pdf->SetFont(PDF_FONT_NAME_MAIN, 'B', 18);
    $pdf->Cell(12, 50, 'PROGRAMACION DIARIA', 0, 0, 'C');

    $pdf->Ln(35);

    $tabla = '';

    foreach ($datos_salones_reserva as $salones) {
        $id_salon  = $salones['id'];
        $nom_salon = $salones['nombre'];

        $tabla .= '
        <table border="1" cellpadding="3" style="font-size:1.12em; width:100%;">
        <thead>
        <tr style="text-align:center; font-weight:bold; font-size:1.25em; text-transform: uppercase;">
        <th scope="col" colspan="5">' . $nom_salon . '</th>
        </tr>
        <tr style="text-align:center; font-weight:bold; color: #000 ;">
        <th scope="col">Apartamento</th>
        <th scope="col">Usuario</th>
        <th scope="col">Fecha reserva</th>
        <th scope="col">Hora reserva</th>
        <th scope="col">Detalle reserva</th>
        </tr>
        </thead>
        <tbody>
        ';

        foreach ($datos_reserva as $reserva) {
            $id_reserva  = $reserva['id_reserva'];
            $nom_salon   = $reserva['salon'];
            $fecha       = $reserva['fecha_reserva'];
            $hora        = $reserva['hora'];
            $usuario     = $reserva['nom_user'];
            $detalle     = $reserva['detalle_reserva'];
            $activo      = $reserva['detalle_activo'];
            $id_salon_re = $reserva['id_salon'];
            $apartamento = $reserva['nom_apa'];

            if ($activo == 1 && $id_salon == $id_salon_re) {
                $tabla .= '
                <tr style="text-align:center; color: #000 ;">
                <td>' . $apartamento . '</td>
                <td>' . $usuario . '</td>
                <td>' . $fecha . '</td>
                <td>' . $hora . '</td>
                <td>' . $detalle . '</td>
                </tr>
                ';
            }
        }

        $tabla .= '
        </tbody>
        </table>
        ';

    }

    $tabla .= '';

    $pdf->SetFont(PDF_FONT_NAME_MAIN, '', 10);
    $pdf->writeHTML($tabla, true, false, true, false, '');

    $nombre_archivo = 'Programacion_diaria' . date('Y_m_d');
    //$carpeta_destino = PUBLIC_PATH_ARCH . 'upload/' . $nombre_archivo;

    $pdf->Output($nombre_archivo . '.pdf', 'I');
    //$pdf->Output($carpeta_destino . '.pdf', 'F');
}
