<?php
date_default_timezone_set('America/Bogota');
define('DS', DIRECTORY_SEPARATOR);
define('ROOT', '..' . DS . '..' . DS . '..');
require_once '..' . DS . '..' . DS . '..' . DS . 'confi' . DS . 'Config.php';
require_once CONTROL_PATH . 'salon' . DS . 'ControlSalones.php';

$instancia = ControlSalon::singleton_salon();
$rs        = $instancia->validarSemanaControl();
$horas     = $instancia->horasRestantesSalonControl();

echo json_encode(['mensaje' => $rs, 'horas_restantes' => $horas]);
