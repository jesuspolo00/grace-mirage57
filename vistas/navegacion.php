<?php
require_once CONTROL_PATH . 'Session.php';
$objss = new Session;
$objss->iniciar();
if (!$_SESSION['rol']) {
  $er    = '2';
  $error = base64_encode($er);
  $salir = new Session;
  $salir->iniciar();
  $salir->outsession();
  header('Location:login?er=' . $error);
  exit();
}
include_once VISTA_PATH . 'cabeza.php';
require_once CONTROL_PATH . 'permisos' . DS . 'ControlPermisos.php';

$instancia_permiso = ControlPermisos::singleton_permisos();

$id_log     = $_SESSION['id'];
$perfil_log = $_SESSION['rol'];
?>
<!-- Sidebar -->
<ul class="navbar-nav bg-white sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?=BASE_URL?>inicio">
    <div class="sidebar-brand-icon">
      <!-- <i class="fas fa-hotel text-haj"></i> -->
      <img src="<?=PUBLIC_PATH?>img/icono.png" class="img-fluid" width="40" alt="">
    </div>
    <div class="sidebar-brand-text mx-3 text-haj mt-3">
      G.R.A.C.E
    </div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">

  <!-- Nav Item - Dashboard -->
  <li class="nav-item active">
    <a class="nav-link" href="<?=BASE_URL?>inicio">
      <i class="fas fa-home text-haj"></i>
      <span class="text-muted">Inicio</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider bg-gray">
    <?php
    $permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 2);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>usuarios/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-users text-haj"></i>
          <span class="text-muted">Usuarios</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 3);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>torres/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-building text-haj"></i>
          <span class="text-muted">Torres</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 4);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>salon/index" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-map-pin text-haj"></i>
          <span class="text-muted">Salones</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 5);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>salon/apartar" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-calendar-alt text-haj"></i>
          <span class="text-muted">Reservas Sociales</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 7);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>salon/reservas" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-calendar-day text-haj"></i>
          <span class="text-muted">Mis reservas</span>
        </a>
      </li>
    <?php }
    $permisos = $instancia_permiso->permisosApartamentosControl($perfil_log, 6);
    if ($permisos) {
      ?>
      <li class="nav-item">
        <a class="nav-link collapsed" href="<?=BASE_URL?>salon/diario" aria-expanded="true" aria-controls="collapseUtilities">
          <i class="fas fa-clock text-haj"></i>
          <span class="text-muted">Programacion diaria</span>
        </a>
      </li>
    <?php }
    ?>
    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block bg-gray">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
      <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>
  <!-- End of Sidebar -->

  <!-- Content Wrapper -->
  <div id="content-wrapper" class="d-flex flex-column">

    <!-- Main Content -->
    <div id="content">

      <!-- Topbar -->
      <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow-none">


        <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
          <i class="fa fa-bars text-haj"></i>
        </button>

        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

          <div class="topbar-divider d-none d-sm-block"></div>

          <!-- Nav Item - User Information -->
          <li class="nav-item dropdown no-arrow">
            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?=$_SESSION['nombre_admin'] . ' ' . $_SESSION['apellido']?></span>
              <img class="img-profile rounded-circle" src="<?=PUBLIC_PATH?>img/user.svg">
            </a>
            <!-- Dropdown - User Information -->
            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
              <a class="dropdown-item" href="<?=BASE_URL?>perfil/index">
                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                Perfil
              </a>
              <div class="dropdown-divider"></div>
              <a class="dropdown-item" href="<?=BASE_URL?>salir">
                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                Cerrar sesion
              </a>
            </div>
          </li>

        </ul>

      </nav>
      <!-- End of Topbar -->

      <!-- Begin Page Content -->
      <div class="container-fluid">

        <?php
        include_once VISTA_PATH . 'script_and_final.php';
      ?>